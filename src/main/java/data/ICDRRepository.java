package data;

import java.util.List;
import Entities.Bill;
import Entities.CDR;
import Entities.Phone_Number;

public interface ICDRRepository {
	List<CDR> getCDRsCalculated();
	List<CDR> getCDRsNotCalculated();
	List<CDR> calculateRateForOne(Phone_Number number);
	List<CDR> calculateRateForAll(List<Phone_Number> number);
	void registerCDR(CDR cdr);
	void showAllCDRs();
	void loadCDRs(List<CDR> cdrs);
	void registerCDRs(String cdrs);
	void cleanListCDRsNotCalculated();
	List<CDR> getCalculatedDateFromCDRs();
	List<CDR> getCDRFilterByDateAndHour(String currentDate, String currentHour);
	Bill getRateForClient(int phoneNumber, String date);
//For test
	void setTarificateDate(String date);
}
