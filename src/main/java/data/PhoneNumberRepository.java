package data;

import java.util.ArrayList;
import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;
import plans.IPlan;

public class PhoneNumberRepository implements IPhoneNumberRepository{

	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>();
	List<Phone_Number> ListNewPhoneNumbers  = new ArrayList<Phone_Number>();

	public PhoneNumberRepository() {
		//is not implement because don't have variable that need initialize
	}
	
	public void registerNewPhoneNumbers(String phoneNumbers) {
		int number=0;
		double balance=0;
		String plan_type = null;
		String FriendNumbers = null;
		String[] PhoneNumbersData = phoneNumbers.split("\\r?\\n|;");
		int i = 0;
		while ( i < PhoneNumbersData.length ) {
		  number=Integer.parseInt(PhoneNumbersData[i]);i++;
		  balance=Double.parseDouble(PhoneNumbersData[i]);i++;
		  plan_type=PhoneNumbersData[i];i++;
		  FriendNumbers=PhoneNumbersData[i];
		  Phone_Number newPhoneNumber = new Phone_Number(number, balance, plan_type, FriendNumbers);
		  ListNewPhoneNumbers.add(newPhoneNumber);
			i++;
		}
	}
	
	public List<Phone_Number> getNewPhoneNumbers(){
		return ListNewPhoneNumbers;
	}
	
	public void cleanNewPhoneNumbers() {
		ListNewPhoneNumbers.clear();
	}
	
	
	@Override
	public List<Phone_Number> getNumbers() {
		return ListPhoneNumbers;
	}
	
	@Override
	public void registerNewNumber(Phone_Number newNumber) {
		ListPhoneNumbers.add(newNumber);
	}

	@Override
	public void registerFriendNumber(int number, int friendNumber) {
		Phone_Number numberDB = getNumber(number);
		if (validateNumber(numberDB)) {
			numberDB.registerFriendNumber(friendNumber);

		}
	}

	@Override
	public Phone_Number getNumber(int number) {
		for (Phone_Number phone_Number : ListPhoneNumbers) {
			if (phone_Number.getTelephone()==number) {
				return phone_Number;
			}
		}
		return null;
	}
	
	private boolean validateNumber(Phone_Number numbeDB)
	{
		if (numbeDB==null) 
			return false;
		return true;
	}


	@Override
	public void loadPhonesNumbers(List<Phone_Number> numbers) {
		ListPhoneNumbers.clear();
		ListPhoneNumbers.addAll(numbers);
	}
	

}
