package data;

import java.util.List;

import Entities.Phone_Number;

public interface IPhoneNumberRepository {
	List<Phone_Number>getNewPhoneNumbers();
	List<Phone_Number> getNumbers();
	void registerNewPhoneNumbers(String phoneNumbers);
	void registerNewNumber(Phone_Number newNumber);
	void registerFriendNumber(int number,int friendNumber);
	void cleanNewPhoneNumbers();
	Phone_Number getNumber(int number);
	void loadPhonesNumbers(List<Phone_Number> numbers);
}
