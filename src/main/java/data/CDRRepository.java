package data;
import java.util.ArrayList;
import java.util.List;
import Entities.Bill;
import Entities.CDR;
import Entities.Phone_Number;

public class CDRRepository implements ICDRRepository{

	List<CDR> listCDRsCalculated = new ArrayList<>(); 
	List<CDR> listCDRsNotCalculated = new ArrayList<>(); 
	CurrentDate currentDates = new CurrentDate();
	private List<CDR> listResult ;
	
	public List<CDR> getCalculatedDateFromCDRs() {
        listResult = new ArrayList<>();
        String currentDate = null;
        String currentHour = null;
        for (CDR cdr : listCDRsCalculated) {
        	if (!cdr.gethourTarification().equals(currentHour) || !cdr.getdateTarification().equals(currentDate))
        	{
        		currentDate = cdr.getdateTarification();
        		currentHour = cdr.gethourTarification();
        		listResult.add(cdr);
        	}
    	}
		return listResult;
	}
	
	public List<CDR> getCDRFilterByDateAndHour(String currentDate, String currentHour) {
        listResult = new ArrayList<>();
        for (CDR cdr : listCDRsCalculated) {
        	if (cdr.gethourTarification().equals(currentHour) && cdr.getdateTarification().equals(currentDate))
        	{
        		listResult.add(cdr);
        	}
    	}
        return listResult;
    }
	
	@Override
	public void loadCDRs(List<CDR> listCDRs) {
		listCDRsCalculated.clear();
		listCDRsCalculated.addAll(listCDRs);
	}
	
	@Override
	public List<CDR> getCDRsCalculated() {
		return listCDRsCalculated;
	}

	@Override
	public List<CDR> getCDRsNotCalculated() {
		return listCDRsNotCalculated;
	}
	
	@Override
	public void registerCDR(CDR cdr) {
		listCDRsNotCalculated.add(cdr);
	}

	@Override
	public void showAllCDRs() {
		for (CDR cdr : listCDRsCalculated) {
			cdr.showInformation();
		}
		for (CDR cdr : listCDRsNotCalculated) {
			cdr.showInformation();
		}
	}

	@Override
	public List<CDR> calculateRateForOne(Phone_Number number) {
		List<CDR> result = searchCDRs(number);
		for (CDR cdr : result) {
			cdr.setCallCost(cdr.calculteCallCost(number));
		}
		return result;
	}

	@Override
	public List<CDR> calculateRateForAll(List<Phone_Number> listPhoneNumber) {
		String currentHour = currentDates.getHour();
		String currentDate = currentDates.getDate();
		for(CDR register : listCDRsNotCalculated)
		{
			Phone_Number phone = searchPhone(register.getOriginNumber(),listPhoneNumber);
			register.setCallCost(register.calculteCallCost(phone));
			register.SetdateTarification(currentDate);
			register.SethourTarification(currentHour);
		}
		return listCDRsNotCalculated;
	}

	public Phone_Number searchPhone(int numberOrigin,List<Phone_Number> listPhoneNumber) {
		for(Phone_Number phone : listPhoneNumber)
		{
			if(phone.getTelephone() == numberOrigin)
				return phone;
		}
		return null;
	}
	
	
	private List<CDR> searchCDRs(Phone_Number number){
        listResult = new ArrayList<>();
		for (CDR cdr : listCDRsNotCalculated) {
			if (cdr.getOriginNumber() ==  number.getTelephone()) {
				listResult.add(cdr);
			}
		}
		return listResult;
	}

	@Override
	public void registerCDRs(String cdrs) {
		int originNumber = 0;
		int destinationNumber = 0;
		String durationCall = null;
		String date = null;
		String hour = null;
		String[] parts = cdrs.split("\\r?\\n|;");
		
		int i=0;
		while (i<parts.length) {
			originNumber=Integer.parseInt(parts[i]);i++;
			destinationNumber=Integer.parseInt(parts[i]);i++;
			durationCall=parts[i];i++;
			date=parts[i];i++;
			hour=parts[i];i++;
			CDR newCDR = new CDR(originNumber, destinationNumber, durationCall, date, hour);
			listCDRsNotCalculated.add(newCDR);
		}
	}

	@Override
	public void cleanListCDRsNotCalculated() {
		listCDRsNotCalculated.clear();
	}

	@Override
	public Bill getRateForClient(int phoneNumber, String date) {
		Bill newBill = new Bill(phoneNumber, date, 0);
		for (CDR cdr : listCDRsCalculated) {
			if (validateNumberAndDate(phoneNumber,date,cdr))
			{
				newBill.setcost(newBill.getcost()+cdr.getCallCost());
			}
		}
		return newBill;
	}
	
	private boolean validateNumberAndDate(int phoneNumber, String date,CDR cdr) {
		if (phoneNumber == cdr.getOriginNumber()  && date.contains(getMonthFromDate(cdr.getdateTarification()))) {
			return true;
	  	} else {
	  		return false;
	  	}
	}
	
	private String getMonthFromDate(String date) {
		String[] str = date.split("/");
		int month = Integer.parseInt(str[1]);
		return String.valueOf(month);
	}


	@Override
	public void setTarificateDate(String date) {
		for (CDR cdr : listCDRsCalculated) {
			cdr.SetdateTarification(date);
		}
	}
}
