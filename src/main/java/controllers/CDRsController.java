package controllers;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import spark.*;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;
import useCases.*;

import static spark.Spark.*;
import com.google.gson.Gson;


public class CDRsController {
	private static final String PERSISTENCE = "Persistence"; 
	private static final String NAVBAR = "velocity/nav.vm";
	private static final String TOTAL_CDRS = "TotalCDRs";
	private static final String TITLE = "Title";
	private static final String ALL_CDRS = "velocity/registers/all.vm";
	private static final String CHANGE_PERSISTENCE_PAGE= "velocity/ChangePersistence/ChangePersistencePage.vm";
	IShowCDRs showCDRs;
	ICalculateCostCDR calculateCostCDR;
	ILoadCDRsFromExternalFile loadCDRsFromExternalFile;
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence;
	public CDRsController(IShowCDRs showCDRs,
						  ICalculateCostCDR calculateCostCDR,
						  ILoadCDRsFromExternalFile loadCDRsFromExternalFile,
						  ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence) {
		this.showCDRs = showCDRs;
		this.calculateCostCDR = calculateCostCDR;
		this.loadCDRsFromExternalFile = loadCDRsFromExternalFile;
		this.loadAndSaveCDRandNumbersPersistence = loadAndSaveCDRandNumbersPersistence;
    	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
	}

	public void methods() {
		//http://localhost:8080/ //For Open the UI
    	//port(8080);
		final Gson gson =new Gson(); 
        get("/", (request, response) -> getInitialPage());
    	
        get("/getCostCDRs", (request, response) -> getCost());
        
        get("/DeleteCDRsLoaded", (request, response) -> deleteCDRsLoaded());
       
        get("/recoverCDRsFromPersistence", (request, response) -> recoverCDRs());

        get("/SaveCDRs", (request, response) -> saveCDRS());
        
        get("/ChangePersistence", (request, response) -> changePersistence());
        
        get("/loadFile", (request, response) -> loadFile());
   
        get("/SQLPersistence", (request, response) -> sqlPersitence());

        get("/FilePersistence", (request, response) -> filePersistence());
        
        post("/api/submit", (req,res)->{
        	Part uploadedFile = getUploadedFile(req);
        	try(InputStream inStream = uploadedFile.getInputStream()){
        		StringWriter writer = new StringWriter();
        		IOUtils.copy(inStream, writer);
        		String theString = writer.toString();
        		loadCDRsFromExternalFile.LoadCDRsFromFileCSV(theString);
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        	return getFileView();
        });

        get("/SearchCDRRegister", (request, response) -> searchCDR());
        
        get("/api/getCDRsFilter", (request, response) ->
        {
        	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
        	//ShowFilterCDRs
        	String date = request.queryParams("myText");
        	String hour = request.queryParams("myHour");
        	return getViewCDRsSearched(date,hour);
		});
		
		get("/getRate/:phoneNumber/date/:date", (request, response) ->
        {
			Integer phoneNumber= Integer.parseInt(request.params(":phoneNumber"));
			String date = request.params(":date");
			return gson.toJson(calculateCostCDR.getCostForClient(phoneNumber,date));
        });
	}

	private Part getUploadedFile(Request req) {
		req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/tmp"));
		Part uploadedFile=null;
		try {
			uploadedFile=req.raw().getPart("myFile");
		}
		catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return uploadedFile;
	}
	
	
	public Object getInitialPage() {
		Map<String, Object> model = new HashMap<>();
    	model.put("Message","Pagina de Inicio");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/index.vm"));
	}
	
	public Object getCost() {
    	calculateCostCDR.CalculateCostForNewCDRs();
    	Map<String, Object> model = new HashMap<>();
    	model.put("CDRs", showCDRs.ShowCDRsNotCalculated());
    	model.put(TOTAL_CDRS, showCDRs.ShowCDRsNotCalculated().size());
    	model.put(TITLE,"�CDRs Tarificados!");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, ALL_CDRS));
	}
	
	public Object deleteCDRsLoaded() {
    	loadCDRsFromExternalFile.CleanCDRsLoaded();
    	Map<String, Object> model = new HashMap<>();
    	model.put("CDRs", showCDRs.ShowCDRsNotCalculated());
    	model.put(TOTAL_CDRS, showCDRs.ShowCDRsNotCalculated().size());
    	model.put(TITLE,"CDRs Eliminados");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, ALL_CDRS));
	}
	
	public Object recoverCDRs() {
    	Map<String, Object> model = new HashMap<>();
    	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
    	model.put("CDRs",showCDRs.ShowCDRsFromPersistence());
    	model.put(TOTAL_CDRS, showCDRs.ShowCDRsFromPersistence().size());
    	model.put(TITLE,"Datos Recuperados");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/CDRsTable.vm"));
	}
	
	public Object saveCDRS() {
    	Map<String, Object> model = new HashMap<>();
    	model.put("Message","�Los CDRs Se Guardaron Con Exito!");
    	calculateCostCDR.CalculateCostForNewCDRs();
    	loadAndSaveCDRandNumbersPersistence.SaveCDRPersistence(showCDRs.ShowCDRsNotCalculated());
    	loadCDRsFromExternalFile.CleanCDRsLoaded();
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/SaveMessage.vm"));
	}
	
	public Object changePersistence() {
       	Map<String, Object> model = new HashMap<>();
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, CHANGE_PERSISTENCE_PAGE));
	}
	
	public Object loadFile(){
    	Map<String, Object> model = new HashMap<>();
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/loadFile/IULoad.vm"));
	}
	
	public Object sqlPersitence() {
		Map<String, Object> model = new HashMap<>();
    	loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SQL_PERSISTENCE");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model, NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, CHANGE_PERSISTENCE_PAGE));
	}
	
	public Object filePersistence() {
      	Map<String, Object> model = new HashMap<>();
    	loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SERIALIZATED_PERSISTENCE");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model, NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, CHANGE_PERSISTENCE_PAGE));
	}
	
	public Object getFileView() {
    	Map<String, Object> model = new HashMap<>();
    	model.put("CDRs", showCDRs.ShowCDRsNotCalculated());
    	model.put(TOTAL_CDRS, showCDRs.ShowCDRsNotCalculated().size());
    	model.put(TITLE,"CDRs Cargados");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model, NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, ALL_CDRS));
	}
	
	public Object searchCDR() {
     	Map<String, Object> model = new HashMap<>();
    	//ShowFilterCDRs
    	loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
    	model.put("CDRs", showCDRs.ShowFilterCDRs());
    	model.put(TITLE, "Buscar por Fecha");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model, NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/SearchForDate.vm"));
	}
	
	public Object getViewCDRsSearched(String date,String hour) {
    	Map<String, Object> model = new HashMap<>();
		model.put("CDRs", showCDRs.ShowCDRsFilterByDateAndHour(date,hour));
    	model.put(TOTAL_CDRS, showCDRs.ShowCDRsFilterByDateAndHour(date,hour).size());
    	model.put(TITLE,"CDRs Encontrados");
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model, NAVBAR));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/CDRsTable.vm"));
    	
	}
	
}
