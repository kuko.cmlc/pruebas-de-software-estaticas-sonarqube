package controllers;
import static spark.Spark.get;
import static spark.Spark.post;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;

import spark.ModelAndView;
import spark.Request;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;
import useCases.ILoadAndSaveCDRandNumbersPersistence;
import useCases.ILoadPhoneNumberFromExternalFile;
import useCases.IShowPhoneNumbers;

public class PhoneNumbersController {
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence;
	IShowPhoneNumbers showPhoneNumbers;
	ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile;
	public PhoneNumbersController(ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence,
									IShowPhoneNumbers showPhoneNumbers,
									ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile
									) {
		this.loadAndSaveCDRandNumbersPersistence = loadAndSaveCDRandNumbersPersistence;
		this.showPhoneNumbers = showPhoneNumbers;
		this.loadPhoneNumberFromExternalFile = loadPhoneNumberFromExternalFile;
		loadAndSaveCDRandNumbersPersistence.LoadPhoneNumbersPersistence();
	}
	private static final String NUMBERS = "Numbers";
	private static final String TOTALPHONENUMBERS = "TotalPhoneNumbers";
	private static final String PERSISTENCE = "Persistence";
	private static final String VELOCITY = "velocity/nav.vm";
	private static final String VELOCITYPREREGISTERPHONES = "velocity/phoneNumbers/PreRegisterPhoneNumbers.vm";
	
	public void methods() {
		//http://localhost:8080/getNumbers //For Open de IU
        get("/getNumbers", (request, response) -> getPhoneNumbers());
        
        get("/loadFileNumbers", (request, response) -> getViewLoadFile());
                
        post("/api/submitPhoneNumbers", (req,res)->{
        	Part uploadedFile = getUploadedFile(req);
        	try(InputStream inStream = uploadedFile.getInputStream()){
        		StringWriter writer = new StringWriter();
        		IOUtils.copy(inStream, writer);
        		String theString = writer.toString();
        		loadPhoneNumberFromExternalFile.LoadPhoneNumbersFromFileCSV(theString);
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        	return getNewPhoneNumbers();
        });

        get("/getNewPhoneNumbers", (request, response) -> getNewPhoneNumbers());        

        get("/DeletePhoneNumbersLoaded", (request, response) -> deletePhoneNumbers());
     
        get("/SaveNewPhoneNumbers", (request, response) -> savePhoneNumbers());
	}

	private Part getUploadedFile(Request req) {
		req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/tmp"));
		Part uploadedFile=null;
		try {
			uploadedFile=req.raw().getPart("myFile");
		}
		catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return uploadedFile;
	}
	
	public Object getPhoneNumbers() {
    	Map<String, Object> model = new HashMap<>();
		loadAndSaveCDRandNumbersPersistence.LoadPhoneNumbersPersistence();
    	model.put(NUMBERS,showPhoneNumbers.ShowPhoneNumbersFromPersistence());
    	model.put(TOTALPHONENUMBERS,showPhoneNumbers.ShowPhoneNumbersFromPersistence().size());        	
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,VELOCITY));
		return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/phoneNumbers/numbers.vm"));
	}
	
	public Object getViewLoadFile() {
    	Map<String, Object> model = new HashMap<>();
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,VELOCITY));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/loadFile/IULoadPhoneNumbers.vm"));
	}
	
	public Object getNewPhoneNumbers() {
       	Map<String, Object> model = new HashMap<>();
    	model.put(NUMBERS, showPhoneNumbers.ShowNewPhoneNumbers());
    	model.put(TOTALPHONENUMBERS,showPhoneNumbers.ShowNewPhoneNumbers().size());
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,VELOCITY));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, VELOCITYPREREGISTERPHONES));
	}
	
	public Object deletePhoneNumbers() {
		Map<String, Object> model = new HashMap<>();
    	loadPhoneNumberFromExternalFile.CleanPhoneNumbersLoaded();
    	model.put(NUMBERS, showPhoneNumbers.ShowNewPhoneNumbers());
    	model.put(TOTALPHONENUMBERS,showPhoneNumbers.ShowNewPhoneNumbers().size());
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,VELOCITY));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, VELOCITYPREREGISTERPHONES));
	}
	
	public Object savePhoneNumbers() {
		Map<String, Object> model = new HashMap<>();
    	model.put("Message","�Los Nuevos numeros telefonicos se Guardaron Con Exito!");
    	loadAndSaveCDRandNumbersPersistence.SavePhoneNumbersPersistence(showPhoneNumbers.ShowNewPhoneNumbers());
    	loadPhoneNumberFromExternalFile.CleanPhoneNumbersLoaded();
    	model.put(PERSISTENCE,loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,VELOCITY));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/SaveMessage.vm"));
	}
}
