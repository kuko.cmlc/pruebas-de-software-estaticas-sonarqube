package Entities;

import java.io.Serializable;
import java.util.ArrayList;
import constructors.Plan_Factory;
import plans.IPlan;

public class Phone_Number implements Serializable {
	private static final long serialVersionUID = -2555170365942325277L;
	private static final int LIMIT_FRIENDS_NUMBERS = 4;
	private int telephone;
	private int antique;
	private double  balance;
	private IPlan plan_rate;
	private ArrayList<Integer>telephoneFriends = new ArrayList<Integer>();
	
	public Phone_Number(int telephone,double balance,String planType, String friends_number) {
		this.telephone = telephone;
        this.balance = balance;
        this.antique = 0;
        getFriendsNumbersFromStringList(friends_number);
        setPlan(planType);
	}
	public int getTelephone(){
		return telephone;
	}
	
	public int getAntique() {
		return antique;
	}

	public double getBalance() {
		return balance;
	}
	
	public String getTelephoneFriends(){
		String result=null;
        if (telephoneFriends.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (int s : telephoneFriends) {
                sb.append(s).append(",");
            }
            result = sb.deleteCharAt(sb.length() - 1).toString();
        }
        else	
        {
        	result = "S/N";
        }
        return result;
	}

	public String getPlan() {
		return plan_rate.getPlanType();
	}
	
	
	public double calculateCost(String hour,String duration,int targetTelephone){
		double result;
		result= plan_rate.calculateCost(hour, duration, targetTelephone);
		return result;
	}

	public void setPlan(String planType) {
		Plan_Factory plan = new Plan_Factory();
		plan_rate = plan.ChooseRate(planType, telephoneFriends);
	} 
	

	public void registerFriendNumber(int telephone) {
		if(telephoneFriends.size()<LIMIT_FRIENDS_NUMBERS)
			telephoneFriends.add(telephone);
	}
	
	public void getFriendsNumbersFromStringList(String friends_number) {
	        if(!friends_number.contentEquals("S/N"))
			{
	            String[] friends = friends_number.split(",");
	            for(int i=0;i<friends.length;i++)
	            {
            		registerFriendNumber(Integer.parseInt(friends[i])); 
	            }
	        }
	    }
}

