package main;
import data.CDRRepository;
import data.ICDRRepository;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;
import useCases.CalculateCostCDR;
import useCases.ICalculateCostCDR;
import useCases.ILoadAndSaveCDRandNumbersPersistence;
import useCases.ILoadCDRsFromExternalFile;
import useCases.ILoadPhoneNumberFromExternalFile;
import useCases.IShowCDRs;
import useCases.IShowPhoneNumbers;
import useCases.LoadAndSaveCDRandNumbersPersistence;
import useCases.LoadCDRsFromExternalFile;
import useCases.LoadPhoneNumbersFromExternalFile;
import useCases.ShowCDRs;
import useCases.ShowPhoneNumbers;
import Persistence.FileSQL;
import Persistence.IDataPersistence;
import controllers.CDRsController;
import controllers.PhoneNumbersController;

public class Main {
	public static void main(String[] args) {		
		//Starting Repositories
		IPhoneNumberRepository PhonesRepository = new PhoneNumberRepository();
		ICDRRepository CDRsRepository = new CDRRepository();
		IDataPersistence DataPersistence = new FileSQL();
		//Starting boundaries - Interactors (User Cases)
		ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence = new LoadAndSaveCDRandNumbersPersistence(DataPersistence,CDRsRepository,PhonesRepository);
		ILoadCDRsFromExternalFile loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(CDRsRepository);
		ICalculateCostCDR calculateCostCDR = new CalculateCostCDR(CDRsRepository, PhonesRepository);
		IShowCDRs showCDRs = new ShowCDRs(CDRsRepository);
		IShowPhoneNumbers showPhoneNumbers = new ShowPhoneNumbers(PhonesRepository);
		ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile = new LoadPhoneNumbersFromExternalFile(PhonesRepository);
		//Creating the Controllers
		CDRsController controllerCDR= new CDRsController(showCDRs, calculateCostCDR, loadCDRsFromExternalFile, loadAndSaveCDRandNumbersPersistence);
		PhoneNumbersController phoneNumbers= new PhoneNumbersController(loadAndSaveCDRandNumbersPersistence,showPhoneNumbers,loadPhoneNumberFromExternalFile);
		controllerCDR.methods();
		phoneNumbers.methods();
	}
}