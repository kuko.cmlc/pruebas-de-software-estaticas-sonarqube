package useCases;

import data.IPhoneNumberRepository;

public class LoadPhoneNumbersFromExternalFile implements ILoadPhoneNumberFromExternalFile{
	IPhoneNumberRepository phoneNumberRepository;
	
	public LoadPhoneNumbersFromExternalFile(IPhoneNumberRepository phoneNumberRepository) {
		this.phoneNumberRepository = phoneNumberRepository;
	}
	
	@Override
	public void LoadPhoneNumbersFromFileCSV(String phoneNumbers) {
		phoneNumberRepository.registerNewPhoneNumbers(phoneNumbers);
	}

	@Override
	public void CleanPhoneNumbersLoaded() {
		phoneNumberRepository.cleanNewPhoneNumbers();
	}

}
