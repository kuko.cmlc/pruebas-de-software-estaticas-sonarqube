package useCases;

import java.util.List;

import data.ICDRRepository;
import Entities.CDR;

public class LoadCDRsFromExternalFile implements ILoadCDRsFromExternalFile {
	
	ICDRRepository CDRRepository;
	public LoadCDRsFromExternalFile(ICDRRepository CDRRepository) {
		this.CDRRepository = CDRRepository;		
	}

	public void LoadCDRsFromFileCSV(String ListCDR) {
		CDRRepository.registerCDRs(ListCDR);
	}

	@Override
	public void CleanCDRsLoaded() {
		CDRRepository.cleanListCDRsNotCalculated();
	}

}
