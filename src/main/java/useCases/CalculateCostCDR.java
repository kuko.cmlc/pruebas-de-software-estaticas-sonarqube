package useCases;

import java.util.List;

import data.ICDRRepository;
import data.IPhoneNumberRepository;
import Entities.CDR;
import Entities.Bill;
import Entities.Phone_Number;

public class CalculateCostCDR implements ICalculateCostCDR{

	ICDRRepository cdrRepository;
	IPhoneNumberRepository PhoneNumberRepository;
	public CalculateCostCDR(ICDRRepository cdrRepository,IPhoneNumberRepository PhoneNumberRepository) {
		this.cdrRepository = cdrRepository;
		this.PhoneNumberRepository = PhoneNumberRepository;
	}
	@Override
	public void CalculateCostForNewCDRs() {
		cdrRepository.calculateRateForAll(PhoneNumberRepository.getNumbers());
	}
	@Override
	public void SetData(List<Phone_Number> List_Numbers) {
		PhoneNumberRepository.loadPhonesNumbers(List_Numbers);
	}
	@Override
	public Bill getCostForClient(int PhoneNumber,String date) {
		return cdrRepository.getRateForClient(PhoneNumber,date);
	}
}
