package useCases;

import java.util.List;

import data.IPhoneNumberRepository;
import Entities.Phone_Number;

public class ShowPhoneNumbers implements IShowPhoneNumbers{

	IPhoneNumberRepository PhoneNumberRepository;
	public ShowPhoneNumbers(IPhoneNumberRepository PhoneNumberRepository) {
		this.PhoneNumberRepository = PhoneNumberRepository;
	}
	@Override
	public List<Phone_Number> ShowPhoneNumbersFromPersistence() {
		return PhoneNumberRepository.getNumbers();
	}
	@Override
	public List<Phone_Number> ShowNewPhoneNumbers() {
		return PhoneNumberRepository.getNewPhoneNumbers();
	}
}
