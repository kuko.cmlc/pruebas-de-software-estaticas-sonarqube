package useCases;

import java.time.Period;
import java.util.List;

import constructors.PersistenceFactory;
import data.ICDRRepository;
import data.IPhoneNumberRepository;
import Entities.CDR;
import Entities.Phone_Number;
import Persistence.IDataPersistence;

public class LoadAndSaveCDRandNumbersPersistence implements ILoadAndSaveCDRandNumbersPersistence{
	PersistenceFactory factory = new PersistenceFactory();
	IDataPersistence Persistence;
	ICDRRepository CDRRepository; 
	IPhoneNumberRepository PhoneNumberRepository;
	public LoadAndSaveCDRandNumbersPersistence(IDataPersistence persistence, ICDRRepository CDRRepository, IPhoneNumberRepository PhoneNumberRepository) {
		 this.Persistence = persistence;
		 this.CDRRepository = CDRRepository;
		 this.PhoneNumberRepository = PhoneNumberRepository;
	}
	@Override
	
	public void SaveCDRPersistence(List<CDR> ListCDRsCalculated) {
		Persistence.saveDataCDRs(ListCDRsCalculated);
	}

	@Override
	public void LoadCDRPersistence() {
		CDRRepository.loadCDRs(Persistence.loadDataCDRs());
	}

	@Override
	public void LoadPhoneNumbersPersistence() {
		PhoneNumberRepository.loadPhonesNumbers(Persistence.loadDataPhoneNumbers());
	}
	@Override
	public void ChoosePersistence(String type) {
		this.Persistence = factory.changePersistence(type);		
	}
	@Override
	public void SavePhoneNumbersPersistence(List<Phone_Number> ListPhoneNumbers) {
		this.Persistence.saveDataPhoneNumbers(ListPhoneNumbers);
	}
	@Override
	public String GetTypePersistence() {
		return Persistence.getTypePersistence();
	}

}
