package useCases;

public interface ILoadCDRsFromExternalFile {
	void LoadCDRsFromFileCSV(String ListCDR);
	void CleanCDRsLoaded();
}
