package useCases;

import java.util.List;
import Entities.Phone_Number;

public interface IShowPhoneNumbers {
	List<Phone_Number> ShowPhoneNumbersFromPersistence();
	List<Phone_Number> ShowNewPhoneNumbers();
}
 