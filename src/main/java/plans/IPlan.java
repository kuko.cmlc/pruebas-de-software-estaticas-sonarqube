package plans;

public interface IPlan{
	TimeController betweenTime = new TimeController();
	double calculateCost(String hour, String duration, int targetTelephone);
	String getPlanType();
}
