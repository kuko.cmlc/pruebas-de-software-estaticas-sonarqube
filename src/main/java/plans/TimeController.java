package plans;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.logging.*;

public class TimeController implements Serializable {
  private static final long serialVersionUID = 1905122041950251207L;
	private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
	private LocalTime initialTime;
	private LocalTime finalTime;
	private LocalTime intervalOfTime;
	Logger logger = Logger.getLogger(TimeController.class.getName());
	public boolean isBetweenTime(String initialTimeParameter,String finalTimeParameter, String intervalOfTimeParameter) 
	{
		transformTime(initialTimeParameter,finalTimeParameter,intervalOfTimeParameter);
		if (intervalOfTime.isBefore(finalTime))
			return initialTime.isAfter(finalTime) || initialTime.isBefore(intervalOfTime);
		else
			return initialTime.isAfter(finalTime) && initialTime.isBefore(intervalOfTime);
	}
	
	@SuppressWarnings("deprecation")
	public Boolean transformTime(String initialTimeParameter,String finalTimeParameter, String intervalOfTimeParameter) {
        try
        {
          java.util.Date initialDate = sdf.parse(initialTimeParameter); 
          java.util.Date finalDate = sdf.parse(finalTimeParameter); 
          java.util.Date intervalDate = sdf.parse(intervalOfTimeParameter); 
          
          initialTime = LocalTime.of(initialDate.getHours(), initialDate.getMinutes(),initialDate.getSeconds());
          finalTime = LocalTime.of(finalDate.getHours(), finalDate.getMinutes(),finalDate.getSeconds());
          intervalOfTime = LocalTime.of(intervalDate.getHours(), intervalDate.getMinutes(),intervalDate.getSeconds());      
          return true;
        } 
        catch (Exception e)
        {
        	logger.log(Level.INFO,"Error transform Time ::", e);
        	return false;
        }
	}

	@SuppressWarnings("deprecation")
	public double convertHourToSeconds(String hour)
	{
		double totalSeconds;
	    try
	    {
	    	java.util.Date date = sdf.parse(hour);        
	    	totalSeconds = ((date.getHours() * 3600) + (date.getMinutes() * 60) + (date.getSeconds()));
	    	return totalSeconds;
	    } 
	    catch (Exception e) 
	    {
				logger.log(Level.INFO,"Error convert to Hour to seconds", e);
	    }
	    return 0;
	}	
}
