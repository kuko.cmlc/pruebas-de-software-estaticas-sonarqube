package constructors;

import java.util.ArrayList;

import plans.IPlan;
import plans.Plan_PostPago;
import plans.Plan_Prepago;
import plans.Plan_Wow;

public class Plan_Factory {

	public IPlan ChooseRate(String type,ArrayList<Integer> telephoneFriends) {
		IPlan rateType = null;
		if ("PLAN_POSTPAGO".equals(type)) 
            rateType = new Plan_PostPago();
        if ("PLAN_PREPAGO".equals(type)) 
            rateType = new Plan_Prepago();
        if ("PLAN_WOW".equals(type)) 
            rateType = new Plan_Wow(telephoneFriends);
		return rateType;
	}
}
