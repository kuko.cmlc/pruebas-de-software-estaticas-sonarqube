package constructors;

import Persistence.*;
import Persistence.IDataPersistence;

public class PersistenceFactory {

	public IDataPersistence changePersistence(String type) {
		IDataPersistence persistenceType = null;
		if ("SQL_PERSISTENCE".equals(type)) 
			persistenceType = new FileSQL();
        if ("SERIALIZATED_PERSISTENCE".equals(type)) 
        	persistenceType = new FileTxt();	
        return persistenceType;
	}
}
