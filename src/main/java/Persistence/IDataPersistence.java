package Persistence;

import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;

public interface IDataPersistence {
	void saveDataPhoneNumbers(List<Phone_Number> listPhoneNumbers);
	void saveDataCDRs(List<CDR> ListCDR);
	List<CDR> loadDataCDRs();
	List<Phone_Number> loadDataPhoneNumbers();
	String getTypePersistence();
	void setUrlOrDirectoryFile(String newUrl);
}
