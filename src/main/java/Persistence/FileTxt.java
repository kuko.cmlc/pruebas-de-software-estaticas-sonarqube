package Persistence;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import Entities.CDR;
import Entities.Phone_Number;
import java.io.FileInputStream;
public class FileTxt implements IDataPersistence {
	private static String FILEPATHCDRSSERIALIZATED=System.getProperty("user.dir").toString() + "\\CDRSerializated.txt";
	private static String FILEPATHPHONENUMBERSSERIALLIZATED=System.getProperty("user.dir").toString() + "\\PhoneNumbersSerializated.txt";
	private static String FILEPHONENUMBERSNAME = "PhoneNumbersSerializated.txt";
	private static String FILECDRSNAME = "CDRSerializated.txt";
	
	@Override
	public void saveDataCDRs(List<CDR> CDRs) {
		List<CDR> listCDRsFromFile = loadDataCDRs();
		try (FileOutputStream file = new FileOutputStream(FILECDRSNAME);
			ObjectOutputStream out = new ObjectOutputStream(file);) {	
			out.writeObject(CDRs);
			out.close();
			file.close();
		}catch (Exception ex) {
			ex.getMessage();
		}
	}

	@Override
	public void setUrlOrDirectoryFile(String newUrl) {
		FILEPATHCDRSSERIALIZATED = newUrl;
		FILEPATHPHONENUMBERSSERIALLIZATED = newUrl;
		FILEPHONENUMBERSNAME = newUrl;
		FILECDRSNAME = newUrl;
	}
	
	@Override
	public List<CDR> loadDataCDRs() {
	    FileTxt objectIO = new FileTxt();
        List<CDR> CDRsLoaded = (List<CDR>) objectIO.ReadObjectFromFile(FILEPATHCDRSSERIALIZATED);
		return CDRsLoaded;
	}

	@Override
	public List<Phone_Number> loadDataPhoneNumbers() {
	    FileTxt objectIO = new FileTxt();
        List<Phone_Number> ListPhoneNumber = (List<Phone_Number>) objectIO.ReadObjectFromFile(FILEPATHPHONENUMBERSSERIALLIZATED);
		return ListPhoneNumber;
	}
	
    private Object ReadObjectFromFile(String filepath) {
        try (FileInputStream fileIn = new FileInputStream(filepath);
             ObjectInputStream objectIn = new ObjectInputStream(fileIn);){
        	Object obj = objectIn.readObject();
            objectIn.close();
            return obj;
        } catch (Exception ex) {
            return null;
        }
    }

	@Override
	public void saveDataPhoneNumbers(List<Phone_Number> ListPhoneNumbers) {
		try (FileOutputStream file = new FileOutputStream(FILEPHONENUMBERSNAME);
			ObjectOutputStream out = new ObjectOutputStream(file);){
			out.writeObject(ListPhoneNumbers);
			out.close();
			file.close();
		} catch (IOException ex) {
			ex.getMessage();
		}
	}

	@Override
	public String getTypePersistence() {
		return "SERIALIZATED_PERSISTENCE";
	}
}