package Persistence;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import Entities.CDR;
import Entities.Phone_Number;
import java.util.Properties;
import java.util.logging.*;

public class FileSQL implements IDataPersistence {
    Properties SQLproperties = new Properties();
    String url;
    String user;
    String pwd;
	String SqlInsertIntoPhoneNumber;
	String SqlInsertIntoCDRsTable;
	Logger logger = Logger.getLogger(FileSQL.class.getName());
	public FileSQL() {
		SQLproperties = getProperties("config.properties");
		url = SQLproperties.getProperty("db.url");
		user = SQLproperties.getProperty("db.user");
		pwd = SQLproperties.getProperty("db.password");
		SqlInsertIntoPhoneNumber = "insert into table_phone_numbers (telephone, balance, planType, friends_number)" + " values (?, ?, ?, ?)";
		SqlInsertIntoCDRsTable = "insert into table_cdr (originNumber, destinationNumber, durationCall, date, hour,callCost,dateTarification,hourTarification)"+"value(?,?,?,?,?,?,?,?)";
	}

	@Override
	public void setUrlOrDirectoryFile(String newUrl) {
		this.url = newUrl;
	}
	
	public Properties getProperties(String nameFile) {
		Properties prop = new Properties();
		try (InputStream input = FileSQL.class.getClassLoader().getResourceAsStream(nameFile)) {
					if (input == null)
						return null;
					//load a properties file from class path, inside static method
					prop.load(input);
					return prop;

        } catch (Exception ex) {
        	logger.log(Level.INFO,"Error props", ex);
        }
		return prop;
	}
	
	@Override
	public void saveDataCDRs(List<CDR> ListCDR) {
		try (Connection myConn = DriverManager.getConnection(url,user,pwd);
			Statement myStmt = myConn.createStatement();
			PreparedStatement preparedStmt = myConn.prepareStatement(SqlInsertIntoCDRsTable);){
				for (CDR cdr : ListCDR) {
					preparedStmt.setInt (1,cdr.getOriginNumber());
					preparedStmt.setInt (2,cdr.getDestinationNumber());
					preparedStmt.setString (3,cdr.getDurationCall());
					preparedStmt.setString(4,cdr.getDate());
					preparedStmt.setString(5,cdr.getHour());
					preparedStmt.setDouble(6,cdr.getCallCost());			      
					preparedStmt.setString(7,cdr.getdateTarification());
					preparedStmt.setString(8,cdr.gethourTarification());
					preparedStmt.execute();	
				}		
		}
		catch (Exception exc) {
        	logger.log(Level.INFO,"Error SaveCDRs", exc);
		}		
	}

	@Override
	public List<CDR> loadDataCDRs() {
		List<CDR> ListCDRs = new ArrayList<CDR>();
		try (Connection myConn = DriverManager.getConnection(url,user,pwd);
			Statement myStmt = myConn.createStatement();
			ResultSet myRss = myStmt.executeQuery("select * from table_cdr");){
			while (myRss.next()) {
				CDR register = new CDR(myRss.getInt("originNumber"),myRss.getInt("destinationNumber"),myRss.getString("durationCall"),myRss.getString("date"),myRss.getString("hour"));
				register.SetdateTarification(myRss.getString("dateTarification"));
				register.SethourTarification(myRss.getString("hourTarification"));
				register.setCallCost(myRss.getDouble("callCost"));
				ListCDRs.add(register);
			}
	        return ListCDRs;

		}
		catch (Exception exc) {
        	logger.log(Level.INFO,"Error LoadCDRs", exc);
		}
		return ListCDRs;
	}

	@Override
	public List<Phone_Number> loadDataPhoneNumbers() {
		List<Phone_Number> ListNumbers = new ArrayList<Phone_Number>();
		try (Connection myConn = DriverManager.getConnection(url,user,pwd);
			 Statement myStmt = myConn.createStatement();
		     ResultSet myRs = myStmt.executeQuery("select * from table_phone_numbers");){
			while (myRs.next()) {
	        	Phone_Number phone = new Phone_Number(myRs.getInt("telephone"),myRs.getDouble("balance"),myRs.getString("planType"),myRs.getString("friends_number"));
                ListNumbers.add(phone);
          }
	        return ListNumbers;
		}
		catch (Exception exc) {
        	logger.log(Level.INFO,"Error LoadNumbers", exc);
		}
		return ListNumbers;
	}

	@Override
	public void saveDataPhoneNumbers(List<Phone_Number> ListPhoneNumbers) {
		try (Connection myConn = DriverManager.getConnection(url,user,pwd);
			 Statement myStmt = myConn.createStatement();
			 PreparedStatement preparedStmt = myConn.prepareStatement(SqlInsertIntoPhoneNumber);){
				for (Phone_Number phoneNumber : ListPhoneNumbers) {   
					preparedStmt.setInt (1,phoneNumber.getTelephone());
					preparedStmt.setDouble(2,phoneNumber.getBalance());
					preparedStmt.setString(3,phoneNumber.getPlan());
					preparedStmt.setString(4,phoneNumber.getTelephoneFriends());
					preparedStmt.execute();	
			}
		}
		catch (Exception exc) {
        	logger.log(Level.INFO,"Error SaveNumbers", exc);
		}		

	}

	@Override
	public String getTypePersistence() {
		return "SQL_PERSISTENCE";
	}

}