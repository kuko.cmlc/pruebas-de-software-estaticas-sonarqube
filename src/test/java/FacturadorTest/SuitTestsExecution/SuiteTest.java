package FacturadorTest.SuitTestsExecution;

import org.junit.runner.RunWith;



import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import FacturadorTest.constructorsTest.*;
import FacturadorTest.controllersTest.*;
import FacturadorTest.dataTest.*;
import FacturadorTest.EntitiesTest.*;
import FacturadorTest.MainTest.*;
import FacturadorTest.PersistenceTest.*;
import FacturadorTest.plansTest.*;
import FacturadorTest.useCasesTest.*;

@RunWith(Suite.class)
@SuiteClasses({
	CalculateCostCDRTest.class,
	LoadAndSaveCDRandNumbersPersistenceTest.class,
	LoadCDRsFromExternalFileTest.class,
	LoadPhoneNumbersFromExternalFileTest.class,
	ShowCDRsTest.class,
	ShowPhoneNumbersTest.class,
	Plan_PostPagoTest.class,
	Plan_PrepagoTest.class,
	Plan_WowTest.class,
	TimeControllerTest.class,
	FileSQLTest.class,
	FileTxtTest.class,
	MainTest.class,
	BillTest.class,
	CDRTest.class,
	Phone_NumberTest.class,
	CDRRepositoryTest.class,
	CurrentDateTest.class,
	PhoneNumberRepositoryTest.class,
	CDRsControllerTest.class,
	PhoneNumbersControllerTest.class,
	PersistenceFactoryTest.class,
	Plan_FactoryTest.class
})
public class SuiteTest {

}
