package FacturadorTest.dataTest;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Entities.Bill;
import Entities.CDR;
import Entities.Phone_Number;
import data.CDRRepository;

public class CDRRepositoryTest {
	CDRRepository cdrRepository;
	CDR cdrEntity;
	List<CDR> ListCDRs;
	Phone_Number phoneEntity;
	@Before
	public void initialize()
	{
		cdrRepository = new CDRRepository();
		cdrEntity = new CDR(321,1,"00:01:27", "10/11/19", "00:02:45");
    	ListCDRs = new ArrayList<CDR>();
    	ListCDRs.add(cdrEntity);
    	phoneEntity = new Phone_Number(321, 10, "PLAN_WOW", "2,3");
	}
	@Test
	public void testLoadCDRs() {
    	cdrRepository.loadCDRs(ListCDRs);
	}
	@Test
	public void testGetCalculatedDateFromCDRs() {
		ListCDRs.get(0).SetdateTarification("10/11/19");
		cdrRepository.loadCDRs(ListCDRs);
		assertEquals(ListCDRs.getClass(),cdrRepository.getCalculatedDateFromCDRs().getClass());
	}
	@Test
	public void testGetCDRFilterByDateAndHour() {
		
		ListCDRs.get(0).SethourTarification("00:01:27");
		ListCDRs.get(0).SetdateTarification("10/11/19");
		cdrRepository.loadCDRs(ListCDRs);
		assertEquals(ListCDRs.getClass(),cdrRepository.getCDRFilterByDateAndHour("10/11/19","00:01:27").getClass());
	}
	@Test
	public void testGetCDRsCalculated() {
		assertEquals(ListCDRs.getClass(),cdrRepository.getCDRsCalculated().getClass());
	}
	@Test
	public void testGetCDRsNotCalculated() {
		assertEquals(ListCDRs.getClass(),cdrRepository.getCDRsNotCalculated().getClass());
	}
	@Test
	public void testRegisterCDR() {
		cdrRepository.registerCDR(ListCDRs.get(0));
	}
	@Test
	public void testShowAllCDRs() {
		cdrRepository.loadCDRs(ListCDRs);
		cdrRepository.registerCDR(ListCDRs.get(0));
		cdrRepository.showAllCDRs();
	}
	@Test
	public void testCalculateRateForOne() {
		cdrRepository.registerCDR(ListCDRs.get(0));
		assertEquals(ListCDRs.getClass(),cdrRepository.calculateRateForOne(phoneEntity).getClass());
	}
	@Test
	public void testCalculateRateForAll() {
		List<Phone_Number> listPhoneNumbers=new ArrayList<Phone_Number>();
		listPhoneNumbers.add(new Phone_Number(321, 12, "PLAN_WOW", "2,4"));
		cdrRepository.registerCDR(ListCDRs.get(0));
		assertEquals(ListCDRs.getClass(),cdrRepository.calculateRateForAll(listPhoneNumbers).getClass());
	}
	@Test
	public void testSearchPhone() {
		List<Phone_Number> listPhoneNumbers=new ArrayList<Phone_Number>();
		listPhoneNumbers.add(new Phone_Number(3212, 12, "PLAN_WOW", "2,4"));
		assertNull(cdrRepository.searchPhone(321,listPhoneNumbers));
	}
	@Test
	public void testRegisterCDRs() {
		cdrRepository.registerCDRs("1;2;00:01:27;10/11/19;00:02:45");
	}
	@Test
	public void testCleanListCDRsNotCalculated() {
		cdrRepository.cleanListCDRsNotCalculated();
	}
	@Test
	public void testGetRateForClient() {
		ListCDRs.get(0).SetdateTarification("10/11/19");
		cdrRepository.loadCDRs(ListCDRs);
		Bill newBill = new Bill(321, "10/11/19", 0);
		assertEquals(newBill.getClass(),cdrRepository.getRateForClient(321, "10/11/19").getClass());
		assertEquals(newBill.getClass(),cdrRepository.getRateForClient(3212, "10/11/19").getClass());
	}
	@Test
	public void testSetTarificateDate() {
		cdrRepository.loadCDRs(ListCDRs);
		cdrRepository.setTarificateDate("11/09/20");
		assertEquals("11/09/20",cdrRepository.getCDRsCalculated().get(0).getdateTarification());
	}
	
	
	
}
