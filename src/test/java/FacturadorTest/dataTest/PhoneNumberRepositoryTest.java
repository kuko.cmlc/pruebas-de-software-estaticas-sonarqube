package FacturadorTest.dataTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Entities.Phone_Number;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;

public class PhoneNumberRepositoryTest {
	IPhoneNumberRepository phoneNumberRepository;
	List<Phone_Number> ListPhoneNumbers;
	Phone_Number phoneEntity;
	@Before
	public void initialize()
	{
		phoneNumberRepository=new PhoneNumberRepository();
		ListPhoneNumbers  = new ArrayList<Phone_Number>();
		phoneEntity = new Phone_Number(1, 10, "PLAN_WOW", "2,3");
	}
	
	@Test
	public void testRegisterNewPhoneNumbers() {
		phoneNumberRepository.registerNewPhoneNumbers("1;10;PLAN_PREPAGO;S/N");
		assertTrue(phoneNumberRepository.getNewPhoneNumbers().size()>0);
	}
	@Test
	public void testCleanNewPhoneNumbers() {
		phoneNumberRepository.cleanNewPhoneNumbers();
		assertTrue(phoneNumberRepository.getNewPhoneNumbers().size()==0);
	}
	@Test
	public void testGetNumbers() {
		assertEquals(ListPhoneNumbers.getClass(),phoneNumberRepository.getNumbers().getClass());
	}
	@Test
	public void testRegisterNewNumber() {
		phoneNumberRepository.registerNewNumber(phoneEntity);
		assertTrue(phoneNumberRepository.getNumbers().size()>0);
	}
	@Test
	public void testRegisterFriendNumber() {
		phoneNumberRepository.registerNewNumber(phoneEntity);
		phoneNumberRepository.registerFriendNumber(1, 23);
		phoneNumberRepository.registerFriendNumber(12, 23);
	}
	@Test
	public void testRegisterFriendNumbervalidateNumberNull() {
		phoneNumberRepository.registerNewNumber(phoneEntity);
		phoneNumberRepository.registerFriendNumber(12, 23);
	}
	@Test
	public void testGetNumber() {
		phoneNumberRepository.registerNewNumber(phoneEntity);
		assertNull(phoneNumberRepository.getNumber(2));
	}
	@Test
	public void testLoadPhonesNumbers() {
		ListPhoneNumbers.add(phoneEntity);
		phoneNumberRepository.loadPhonesNumbers(ListPhoneNumbers);
		assertTrue(phoneNumberRepository.getNumbers().size()>0);
	}
	
}
