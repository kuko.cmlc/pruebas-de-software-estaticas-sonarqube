package FacturadorTest.dataTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import data.CurrentDate;

public class CurrentDateTest {
	CurrentDate currentDate;
	String example;
	@Before 
	public void initialize()
	{
		currentDate=new CurrentDate();
	}
	@Test
	public void testGetHour() {
		assertNotNull(currentDate.getHour());
	}
	@Test
	public void testGetDate() {
		assertNotNull(currentDate.getDate());
	}
}
