package FacturadorTest.controllersTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Persistence.FileSQL;
import Persistence.IDataPersistence;
import controllers.PhoneNumbersController;
import data.CDRRepository;
import data.ICDRRepository;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;
import useCases.ILoadAndSaveCDRandNumbersPersistence;
import useCases.ILoadPhoneNumberFromExternalFile;
import useCases.IShowPhoneNumbers;
import useCases.LoadAndSaveCDRandNumbersPersistence;
import useCases.LoadPhoneNumbersFromExternalFile;
import useCases.ShowPhoneNumbers;

public class PhoneNumbersControllerTest {
	IPhoneNumberRepository PhonesRepository;
	ICDRRepository CDRsRepository;
	IDataPersistence DataPersistence;
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence;
	IShowPhoneNumbers showPhoneNumbers;
	ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile;
	PhoneNumbersController phoneNumbers;
	String object;
	@Before
	public void inicialize()
	{
		PhonesRepository = new PhoneNumberRepository();
		CDRsRepository = new CDRRepository();
		DataPersistence = new FileSQL();
		loadAndSaveCDRandNumbersPersistence = new LoadAndSaveCDRandNumbersPersistence(DataPersistence,CDRsRepository,PhonesRepository);
		IShowPhoneNumbers showPhoneNumbers = new ShowPhoneNumbers(PhonesRepository);
		ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile = new LoadPhoneNumbersFromExternalFile(PhonesRepository);
		showPhoneNumbers.ShowPhoneNumbersFromPersistence();
		loadAndSaveCDRandNumbersPersistence.SavePhoneNumbersPersistence(showPhoneNumbers.ShowNewPhoneNumbers());
		phoneNumbers= new PhoneNumbersController(loadAndSaveCDRandNumbersPersistence,showPhoneNumbers,loadPhoneNumberFromExternalFile);
		object=new String();
	}
	@Test
	public void testGetPhoneNumbers() {
		assertEquals(object.getClass(),phoneNumbers.getPhoneNumbers().getClass());
	}
	@Test
	public void testGetViewLoadFile() {
		assertEquals(object.getClass(),phoneNumbers.getViewLoadFile().getClass());
	}
	@Test
	public void testGetNewPhoneNumbers() {
		assertEquals(object.getClass(),phoneNumbers.getNewPhoneNumbers().getClass());
	}
	@Test
	public void testDeletePhoneNumbers() {
		assertEquals(object.getClass(),phoneNumbers.deletePhoneNumbers().getClass());
	}
	@Test
	public void testSavePhoneNumbers() {
		assertEquals(object.getClass(),phoneNumbers.savePhoneNumbers().getClass());
	}
}
