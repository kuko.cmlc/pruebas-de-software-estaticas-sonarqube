package FacturadorTest.controllersTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Persistence.FileSQL;
import Persistence.IDataPersistence;
import controllers.CDRsController;
import data.CDRRepository;
import data.ICDRRepository;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;
import useCases.CalculateCostCDR;
import useCases.ICalculateCostCDR;
import useCases.ILoadAndSaveCDRandNumbersPersistence;
import useCases.ILoadCDRsFromExternalFile;
import useCases.IShowCDRs;
import useCases.IShowPhoneNumbers;
import useCases.LoadAndSaveCDRandNumbersPersistence;
import useCases.LoadCDRsFromExternalFile;
import useCases.ShowCDRs;
import useCases.ShowPhoneNumbers;


public class CDRsControllerTest {
	CDRsController cdrs;
	ICDRRepository CDRsRepository;
	IShowCDRs showCDRs;
	IPhoneNumberRepository PhonesRepository;
	ICalculateCostCDR calculateCostCDR;
	ILoadCDRsFromExternalFile loadCDRsFromExternalFile;
	IDataPersistence DataPersistence;
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence;
	IShowPhoneNumbers showPhoneNumbers;
	String objet;
	@Before
	public void iniciialize()
	{
		CDRsRepository = new CDRRepository();
		showCDRs = new ShowCDRs(CDRsRepository);
		PhonesRepository = new PhoneNumberRepository();
		calculateCostCDR = new CalculateCostCDR(CDRsRepository, PhonesRepository);
		loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(CDRsRepository);
		DataPersistence = new FileSQL();
		showPhoneNumbers = new ShowPhoneNumbers(PhonesRepository);
		loadAndSaveCDRandNumbersPersistence = new LoadAndSaveCDRandNumbersPersistence(DataPersistence,CDRsRepository,PhonesRepository);
		showCDRs.ShowCDRsFromPersistence();
    	showCDRs.ShowFilterCDRs();
    	showCDRs.ShowCDRsFilterByDateAndHour("10/11/19", "00:02:45");
		showPhoneNumbers.ShowPhoneNumbersFromPersistence();
		loadAndSaveCDRandNumbersPersistence.SavePhoneNumbersPersistence(showPhoneNumbers.ShowNewPhoneNumbers());
		cdrs= new CDRsController(showCDRs, calculateCostCDR, loadCDRsFromExternalFile, loadAndSaveCDRandNumbersPersistence);
		objet=new String();
	}
	@Test
	public void testGetInitialPage() {
		assertEquals( objet.getClass(),cdrs.getInitialPage().getClass());
	}
	@Test
	public void testGetCost() {
		assertEquals( objet.getClass(),cdrs.getCost().getClass());
	}
	@Test
	public void testDeleteCDRsLoaded() {
		assertEquals( objet.getClass(),cdrs.deleteCDRsLoaded().getClass());
	}
	@Test
	public void testLoadFile() {
		assertEquals( objet.getClass(),cdrs.loadFile().getClass());
	}
	@Test
	public void testChangePersistence() {
		assertEquals( objet.getClass(),cdrs.changePersistence().getClass());
	}
	@Test
	public void testFilePersistence() {
		assertEquals( objet.getClass(),cdrs.filePersistence().getClass());
	}
	@Test
	public void testSqlPersitence() {
		assertEquals( objet.getClass(),cdrs.sqlPersitence().getClass());
	}
	@Test
	public void testSaveCDRS() {
		assertEquals( objet.getClass(),cdrs.saveCDRS().getClass());
	}
	@Test
	public void testGetFileView() {
		assertEquals( objet.getClass(),cdrs.getFileView().getClass());
	}
	@Test
	public void testRecoverCDRs() {
		assertEquals( objet.getClass(),cdrs.recoverCDRs().getClass());
	}
	@Test
	public void testSearchCDR() {
		assertEquals( objet.getClass(),cdrs.searchCDR().getClass());
	}
	@Test
	public void testGetViewCDRsSearched() {
		assertEquals( objet.getClass(),cdrs.getViewCDRsSearched("10/10/1998","20:01:29").getClass());
	}
}
