package FacturadorTest.plansTest;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import plans.IPlan;
import plans.Plan_Prepago;

public class Plan_PrepagoTest {
	IPlan planPrePago;
	private static final double DELTA = 1e-15;
	
	@Before 
	public void createPlanPrePago() {
		planPrePago = new Plan_Prepago();
	}
	
	@Test
	public void calculateCostForNormalPriceTest() {
		assertEquals(1740.0, planPrePago.calculateCost("08:59:59", "20:00:00" , 1), DELTA);
	}
	
	@Test
	public void calculateCostForReducePriceTest() {
		assertEquals(52.25, planPrePago.calculateCost("21:59:59", "00:55:00", 1), DELTA);
	}
	
	@Test
	public void calculateCostForSuperReducePriceTest() {
		assertEquals(252.0, planPrePago.calculateCost("04:59:59", "06:00:00", 1), DELTA);
	}
	
	@Test
	public void getPlanTypeTest() {
		assertEquals("PLAN_PREPAGO", planPrePago.getPlanType());
	}
}
