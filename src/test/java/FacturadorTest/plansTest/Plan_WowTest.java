package FacturadorTest.plansTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import plans.IPlan;
import plans.Plan_Wow;

public class Plan_WowTest {
	private static final double DELTA = 1e-15;
	ArrayList<Integer> friendsNumber = new ArrayList<Integer>();
	IPlan plan_wow;
	@Before
	public void setup (){
		friendsNumber.add(1);
		friendsNumber.add(2);
		friendsNumber.add(3);
		plan_wow = new Plan_Wow(friendsNumber);
	}
	
	@Test
	public void test_calculateCostFriendNumber() {
		double result = plan_wow.calculateCost("00:00:01", "00:00:01", 1);
		assertEquals(0,result,DELTA);
	}
	
	@Test
	public void test_calculateCostNoFriendNumber() {
		double result = plan_wow.calculateCost("00:00:01", "00:00:01", 331);
		assertEquals(0.02,result,DELTA);
	}
	
	@Test
	public void test_getPlanType() {
		assertEquals("PLAN_WOW", plan_wow.getPlanType());
	}
}
