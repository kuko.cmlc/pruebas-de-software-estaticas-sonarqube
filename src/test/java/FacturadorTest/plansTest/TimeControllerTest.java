package FacturadorTest.plansTest;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.Test;

import plans.TimeController;

public class TimeControllerTest {
	private static final double DELTA = 1e-15;
	TimeController timeController = new TimeController();
	
	@Test
	public void test_isBetweenTime() {
		Boolean result = timeController.isBetweenTime("01:00:00", "10:20:00", "02:00:00");
	    assertTrue(result);
	}	
	
	@Test
	public void test_isBetweenTime_version2() {
		Boolean result = timeController.isBetweenTime("00:00:00", "05:20:00", "00:00:00");
	    assertFalse(result);
	}
	
	@Test
	public void test_isBetweenTime_version3() {
		Boolean result = timeController.isBetweenTime("06:00:00","12:00:00", "09:00:00");
		assertTrue(result);
	}	

	
	@Test
	public void test_isBetweenTime_version4() {
		Boolean result = timeController.isBetweenTime("09:00:00", "00:00:00", "08:00:00");
	    assertFalse(result);
	}

	@Test
	public void test_isNotBetweenTime() {
		Boolean result = timeController.isBetweenTime("01:00:00", "10:20:00", "23:00:00");
	    assertFalse(result);
	}

	@Test
	public void test_isSameTime() {
		Boolean result = timeController.isBetweenTime("00:00:00", "00:00:00", "00:00:00");
	    assertFalse(result);
	}

	@Test
	public void test_transformTime() {
        Boolean result = timeController.transformTime("asdqwe", "00:00:00", "23:00:00");
        assertFalse(result);
	}

	@Test
	public void test_convertHourToSecondsError() {
        double result = timeController.convertHourToSeconds("awdqdqw");
        assertEquals(0,result,DELTA);
	}
	
	@Test
	public void test_convertHourToSeconds() {
        double result = timeController.convertHourToSeconds("00:01:01");
        assertEquals(61,result,DELTA);
	}
}
