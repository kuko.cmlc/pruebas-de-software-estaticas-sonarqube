package FacturadorTest.plansTest;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import plans.IPlan;
import plans.Plan_PostPago;

public class Plan_PostPagoTest {
	IPlan planPostPago;
	private static final double DELTA = 1e-15;
	
	@Before 
	public void createPlanPostPago() {
		planPostPago = new Plan_PostPago();
	}
	
	@Test
	public void calculateCostTest() {
		assertEquals(5.5, planPostPago.calculateCost("06:59:59", "00:05:30", 1),DELTA);
	}
	
	@Test
	public void getPlanTypeTest() {
		assertEquals("PLAN_POSTPAGO", planPostPago.getPlanType());
	}

}
