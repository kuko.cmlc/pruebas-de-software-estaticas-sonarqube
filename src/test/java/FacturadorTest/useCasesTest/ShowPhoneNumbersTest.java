package FacturadorTest.useCasesTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Entities.Phone_Number;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;
import useCases.IShowPhoneNumbers;
import useCases.ShowPhoneNumbers;

public class ShowPhoneNumbersTest {
	IShowPhoneNumbers showPhoneNumber;
	IPhoneNumberRepository phoneNumberRepository = new PhoneNumberRepository();
	List<Phone_Number> ListPhoneNumbers = new ArrayList<Phone_Number>();
	
	@Before
	public void setup() {
		ListPhoneNumbers.add(new Phone_Number(123, 0, "PLAN_WOW", "1,2,3"));
		ListPhoneNumbers.add(new Phone_Number(1, 1, "PLAN_PREPAGO","S/N"));
		ListPhoneNumbers.add(new Phone_Number(1, 1, "PLAN_POSTPAGO","S/N"));
		phoneNumberRepository.loadPhonesNumbers(ListPhoneNumbers);
		showPhoneNumber = new ShowPhoneNumbers(phoneNumberRepository);
	}
	
	@Test
	public void test_ShowPhoneNumbersFromPersistence() {
		List<Phone_Number> result = showPhoneNumber.ShowPhoneNumbersFromPersistence();
		assertEquals(3, result.size());
	}	
	
	@Test
	public void test_ShowNewPhoneNumbers() {
		List<Phone_Number> result = showPhoneNumber.ShowNewPhoneNumbers();
		assertEquals(0, result.size());		
	}
}
