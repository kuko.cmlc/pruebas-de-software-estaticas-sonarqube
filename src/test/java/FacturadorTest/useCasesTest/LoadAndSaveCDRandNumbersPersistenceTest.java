package FacturadorTest.useCasesTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import Entities.CDR;
import Entities.Phone_Number;
import Persistence.FileTxt;
import Persistence.IDataPersistence;
import data.CDRRepository;
import data.ICDRRepository;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;
import useCases.ILoadAndSaveCDRandNumbersPersistence;
import useCases.LoadAndSaveCDRandNumbersPersistence;

public class LoadAndSaveCDRandNumbersPersistenceTest {
	List<CDR> cdrs = new ArrayList<CDR>();
	List<Phone_Number> ListPhoneNumbers = new ArrayList<Phone_Number>();
	IDataPersistence Persistence = new FileTxt();
	ICDRRepository cdrRepository =  new CDRRepository();
	IPhoneNumberRepository phoneNumberRepository = new PhoneNumberRepository();
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence  = new LoadAndSaveCDRandNumbersPersistence(Persistence,cdrRepository,phoneNumberRepository);;
	
	@Test
	public void test_SaveCDRPersistence() {
		loadAndSaveCDRandNumbersPersistence.SaveCDRPersistence(cdrs);
	}
	
	@Test
	public void test_LoadCDRPersistence() {
		loadAndSaveCDRandNumbersPersistence.LoadCDRPersistence();
	}
	
	@Test
	public void test_LoadPhoneNumbersPersistence() {
		loadAndSaveCDRandNumbersPersistence.LoadPhoneNumbersPersistence();
	}
	
	@Test
	public void test_ChoosePersistence() {
		loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SERIALIZATED_PERSISTENCE");
	}
	
	@Test
	public void test_SavePhoneNumbersPersistence() {
		loadAndSaveCDRandNumbersPersistence.SavePhoneNumbersPersistence(ListPhoneNumbers);
	}
	
	@Test
	public void test_GetTypePersistence() {
		String result = loadAndSaveCDRandNumbersPersistence.GetTypePersistence();
		assertEquals("SERIALIZATED_PERSISTENCE", result);
	}
	
	@Test
	public void test_GetTypePersistence_SQL() {
		loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SQL_PERSISTENCE");
		String result = loadAndSaveCDRandNumbersPersistence.GetTypePersistence();
		assertEquals("SQL_PERSISTENCE", result);
	}
}
