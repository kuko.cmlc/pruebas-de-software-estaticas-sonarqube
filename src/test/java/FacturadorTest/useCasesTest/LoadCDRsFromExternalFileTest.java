package FacturadorTest.useCasesTest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import Entities.CDR;
import data.CDRRepository;
import data.ICDRRepository;
import useCases.ILoadCDRsFromExternalFile;
import useCases.LoadCDRsFromExternalFile;

public class LoadCDRsFromExternalFileTest {
	ICDRRepository cdrRepository = new CDRRepository();
	ILoadCDRsFromExternalFile loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(cdrRepository); 

	@Test
	public void test_LoadCDRFromFileCSV() {
		String cdrs ="123;321;00:00;1/1/1;00:00;123;321;00:00;1/1/1;00:00;";
		loadCDRsFromExternalFile.LoadCDRsFromFileCSV(cdrs);
		List<CDR> result = cdrRepository.getCDRsNotCalculated();
		assertEquals(2, result.size());
	}
	
	@Test
	public void test_CleanCDRsLoaded() {
		String cdrs ="123;321;00:00;1/1/1;00:00;123;321;00:00;1/1/1;00:00;";
		loadCDRsFromExternalFile.LoadCDRsFromFileCSV(cdrs);
		loadCDRsFromExternalFile.CleanCDRsLoaded();
		List<CDR> result = cdrRepository.getCDRsNotCalculated();
		assertEquals(0, result.size());
	}
}
