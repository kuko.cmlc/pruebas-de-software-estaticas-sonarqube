package FacturadorTest.useCasesTest;

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Entities.Bill;
import Entities.CDR;
import Entities.Phone_Number;
import data.CDRRepository;
import data.ICDRRepository;
import data.IPhoneNumberRepository;
import useCases.CalculateCostCDR;
import useCases.ICalculateCostCDR;

public class CalculateCostCDRTest {
	private static final double DELTA = 1e-15;
	Bill bill_driver = new Bill(123,"10/11/19",0);
	ArrayList<CDR> cdrs = new ArrayList<CDR>();
	ArrayList<Phone_Number> phoneNumbers = new ArrayList<Phone_Number>();
	ICDRRepository cdrRepository = new CDRRepository();
	IPhoneNumberRepository phoneNumberRepository = new data.PhoneNumberRepository();
	ICalculateCostCDR calculateCost;

	@Before
	public void setup() {
		//initialize models (Drivers)
		cdrs.add(new CDR(123, 321, "00:00", "10/11/19","10:30"));
		cdrs.add(new CDR(321, 321, "00:00", "10/11/19","10:30"));
		phoneNumbers.add(new Phone_Number(123, 0, "PLAN_PREPAGO", "S/N"));
		phoneNumbers.add(new Phone_Number(321, 0, "PLAN_PREPAGO", "S/N"));
		cdrRepository.loadCDRs(cdrs);
		phoneNumberRepository.loadPhonesNumbers(phoneNumbers);
		cdrRepository.setTarificateDate("10/11/19");
		calculateCost = new CalculateCostCDR(cdrRepository,phoneNumberRepository);
	}
	
	@Test
	public void test_CalculateCostForNewCDR() {
		calculateCost.CalculateCostForNewCDRs();
	}	
	
	@Test
	public void test_SetData() {
		calculateCost.SetData(phoneNumbers);
	}
	
	private void Compare_Object(Bill expected, Bill Actual) {
		assertEquals(expected.getphoneNumber(), Actual.getphoneNumber());
		assertEquals(expected.getmonth(), Actual.getmonth());
		assertEquals(expected.getcost(), Actual.getcost(),DELTA);
	}
	
	@Test
	public void test_getCostClient() {
		calculateCost.CalculateCostForNewCDRs();
		Bill bill_response = calculateCost.getCostForClient(123, "10/11/19");
		Compare_Object(bill_driver, bill_response);
	}
	
}
