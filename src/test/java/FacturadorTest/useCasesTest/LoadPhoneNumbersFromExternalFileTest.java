package FacturadorTest.useCasesTest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import Entities.Phone_Number;
import data.IPhoneNumberRepository;
import data.PhoneNumberRepository;
import useCases.ILoadPhoneNumberFromExternalFile;
import useCases.LoadPhoneNumbersFromExternalFile;

public class LoadPhoneNumbersFromExternalFileTest {
	IPhoneNumberRepository phoneNumberRepository = new PhoneNumberRepository();
	ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile= new LoadPhoneNumbersFromExternalFile(phoneNumberRepository);
	
	@Test
	public void test_LoadPhoneNumebersFromFileCSV() {
		String PhoneNumber = "11212;0;PLAN_WOW;1,2,3;11111;0;PLAN_POSTPAGO;1,2,3"; 
		loadPhoneNumberFromExternalFile.LoadPhoneNumbersFromFileCSV(PhoneNumber);
		List<Phone_Number> result = phoneNumberRepository.getNewPhoneNumbers();
		assertEquals(2,result.size());
	}
	
	@Test
	public void test_CleanPhoneNumbersLoaded() {
		String PhoneNumber = "11212;0;PLAN_WOW;1,2,3;11111;0;PLAN_POSTPAGO;1,2,3"; 
		loadPhoneNumberFromExternalFile.LoadPhoneNumbersFromFileCSV(PhoneNumber);
		loadPhoneNumberFromExternalFile.CleanPhoneNumbersLoaded();
		List<Phone_Number> result = phoneNumberRepository.getNewPhoneNumbers();
		assertEquals(0,result.size());
	}
}
