package FacturadorTest.useCasesTest;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Entities.CDR;
import data.CDRRepository;
import data.ICDRRepository;
import useCases.IShowCDRs;
import useCases.ShowCDRs;

public class ShowCDRsTest {
	ICDRRepository cdrRepository = new CDRRepository();
	IShowCDRs showCDRs;
	@Before
	public void setup() {
		ArrayList<CDR> cdrs = new ArrayList<CDR>();
		cdrs.add(new CDR(123,321,"00:10","12/12/12","00:00"));
		cdrs.add(new CDR(321,111,"00:10","12/12/12","00:00"));
		cdrs.add(new CDR(444,123,"00:10","10/12/2000","00:00"));
		cdrs.add(new CDR(111,321,"00:10","10/12/2000","00:00"));
		cdrRepository.loadCDRs(cdrs);
		showCDRs = new ShowCDRs(cdrRepository);
	}
	
	@Test
	public void test_showCDRsNotCalculated() {
		List<CDR> result = showCDRs.ShowCDRsNotCalculated();
		assertTrue(result.isEmpty());
	}
	
	@Test
	public void test_showCDRsFromPersistence() {
		List<CDR> result = showCDRs.ShowCDRsFromPersistence();
		assertEquals(4, result.size());
	}
	
	@Test
	public void test_showFilterCDRs() {
		List<CDR> result = showCDRs.ShowFilterCDRs();
		assertFalse(result.isEmpty());
	}
	
	@Test
	public void test_showShowCDRsFilterByDateAndHour() {
		List<CDR> result = showCDRs.ShowCDRsFilterByDateAndHour("NO TARIFICADO","NO TARIFICADO");
		assertEquals(0, result.size());
	}
}
