package FacturadorTest.constructorsTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import constructors.Plan_Factory;
import plans.IPlan;
import plans.Plan_PostPago;
import plans.Plan_Prepago;
import plans.Plan_Wow;
public class Plan_FactoryTest {
	Plan_Factory planFactory;
	IPlan postPago;
	IPlan prePago;
	IPlan planWow;
	ArrayList<Integer> telephoneFriends;
	@Before
	public void InstantiateObjects() {
		planFactory=new Plan_Factory();
		postPago=new Plan_PostPago();
		prePago=new Plan_Prepago();
		planWow=new Plan_Wow(telephoneFriends);
	}
	@Test
	public void TestPlanFactoryPostPago() {
		assertEquals(postPago.getClass(),planFactory.ChooseRate("PLAN_POSTPAGO", telephoneFriends).getClass());
	}
	@Test
	public void TestPlanFactoryPrePago() {
		assertEquals(prePago.getClass(),planFactory.ChooseRate("PLAN_PREPAGO", telephoneFriends).getClass());
	}
	@Test
	public void TestPlanFactoryPreWow() {
		assertEquals(planWow.getClass(),planFactory.ChooseRate("PLAN_WOW", telephoneFriends).getClass());
	}
}
