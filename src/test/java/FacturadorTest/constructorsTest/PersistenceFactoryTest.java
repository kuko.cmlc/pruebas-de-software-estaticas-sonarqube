package FacturadorTest.constructorsTest;
import constructors.PersistenceFactory;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Persistence.FileSQL;
import Persistence.FileTxt;
import Persistence.IDataPersistence;

public class PersistenceFactoryTest {
	PersistenceFactory persistenceFactory;
	IDataPersistence fileSql;
	IDataPersistence fileTxt;
	@Before
	public void InstantiateObjects() {
		persistenceFactory=new PersistenceFactory();
		fileSql=new FileSQL();
		fileTxt=new FileTxt();
	}
	@Test
	public void TestPersistenceFactoryFileSql() {
		assertEquals(fileSql.getClass(),persistenceFactory.changePersistence("SQL_PERSISTENCE").getClass());
	}
	@Test
	public void TestPersistenceFactoryFileTxt()
	{
		assertEquals(fileTxt.getClass(),persistenceFactory.changePersistence("SERIALIZATED_PERSISTENCE").getClass());
	}
}
