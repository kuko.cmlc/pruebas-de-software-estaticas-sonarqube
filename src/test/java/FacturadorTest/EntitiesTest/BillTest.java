package FacturadorTest.EntitiesTest;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import Entities.Bill;

public class BillTest {
	private static final double DELTA = 1e-15;
	Bill billEntity; 

	@Before 
	public void createEntities() {
		billEntity = new Bill(12345678, "13/05/12", 24);
	}
	
	@Test
	public void getphoneNumberTest() {
		assertEquals(12345678, billEntity.getphoneNumber());
	}

	@Test
	public void getcostTest() {
		assertEquals(24.0, billEntity.getcost(),DELTA);
	}

	@Test
	public void getmonthTest() {
		assertEquals("13/05/12", billEntity.getmonth());
	}
	
	@Test
	public void setcostTest() {
		billEntity.setcost(2);
		assertEquals(2, billEntity.getcost(),DELTA);
	}
	
	@Test
	public void setphoneNumberTest() {
		billEntity.setphoneNumber(123);
		assertEquals(123, billEntity.getphoneNumber());
	}

	@Test
	public void setmonthTest() {
		billEntity.setmonth("12");
		assertEquals("12", billEntity.getmonth());
	}
}
