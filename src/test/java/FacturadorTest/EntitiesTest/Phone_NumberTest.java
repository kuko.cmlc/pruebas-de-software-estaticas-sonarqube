package FacturadorTest.EntitiesTest;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import Entities.Phone_Number;

public class Phone_NumberTest {
	Phone_Number phoneEntity,phoneWithNoFriendsNumbersEntity,phoneWithManyNumbersEntity;
	private static final double DELTA = 1e-15;

	@Before
	public void createEntities() {
		phoneEntity = new Phone_Number(123456, 10, "PLAN_WOW", "2,3");
		phoneWithNoFriendsNumbersEntity = new Phone_Number(789, 0, "PLAN_PREPAGO", "S/N");
		phoneWithManyNumbersEntity = new Phone_Number(123456, 10, "PLAN_WOW", "1,2,3,4");
	}
	
	@Test
	public void getTelephoneTest() {
		assertEquals(123456, phoneEntity.getTelephone());
	}
	
	@Test
	public void getAntiqueTest() {
		assertEquals(0, phoneEntity.getAntique());
	}

	@Test
	public void getBalanceTest() {
		assertEquals(10, phoneEntity.getBalance(),DELTA);
	}
	
	@Test
	public void getTelephoneFriendsTest(){
		assertEquals("2,3", phoneEntity.getTelephoneFriends());
	}

	@Test
	public void getPlanTest() {
		assertEquals("PLAN_WOW", phoneEntity.getPlan());
	}
	
	@Test
	public void calculateCostTest() {
		assertEquals(2.48, phoneEntity.calculateCost("00:03:45", "00:02:30", 1), DELTA);
	}

	@Test
	public void setPlanTest() {
		phoneEntity.setPlan("PLAN_PREPAGO");
		assertEquals("PLAN_PREPAGO", phoneEntity.getPlan());
	} 
	
	@Test
	public void registerFriendNumberTest() {
		phoneEntity.registerFriendNumber(758);
		assertEquals("2,3,758", phoneEntity.getTelephoneFriends());
	}
	
	@Test
	public void registerFiveFriendNumberSTest() {
		phoneWithManyNumbersEntity.registerFriendNumber(5);
		assertEquals("1,2,3,4", phoneWithManyNumbersEntity.getTelephoneFriends());
	}
	
	@Test
	public void phoneHasTelephoneFriendsTest() {
		assertEquals("S/N", phoneWithNoFriendsNumbersEntity.getTelephoneFriends());
	}
}
