package FacturadorTest.EntitiesTest;

import static junit.framework.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Entities.CDR;
import Entities.Phone_Number;

public class CDRTest {
	CDR cdrEntity; 
	Phone_Number phoneEntity; 

	@Before 
	public void createEntities() {
		cdrEntity = new CDR(456,1,"00:02:30", "12/11/19", "00:03:45");
		phoneEntity = new Phone_Number(1, 10, "PLAN_WOW", "2,3");
	}
	
	@Test
	public void getdateTarificationTest() {
		assertEquals("NO TARFICADO", cdrEntity.getdateTarification());
	} 
	
	@Test
	public void gethourTarificationTest() {
		assertEquals("NO TARFICADO", cdrEntity.gethourTarification());
	} 
	
	@Test
	public void getDurationCallTest() {
		assertEquals("00:02:30", cdrEntity.getDurationCall());
	} 
	
	@Test
	public void getDateTest() {
		assertEquals("12/11/19", cdrEntity.getDate());
	} 
	
	@Test
	public void getHourTest() {
		assertEquals("00:03:45", cdrEntity.getHour());
	} 
	
	@Test
	public void SetdateTarificationTest() {
		cdrEntity.SetdateTarification("01/12/19");
		assertEquals("01/12/19", cdrEntity.getdateTarification());
	}
	
	@Test
	public void SethourTarificationTest() {
		cdrEntity.SethourTarification("01:10:45");
		assertEquals("01:10:45", cdrEntity.gethourTarification());
	} 
	
	@Test
	public void setCallCostTest() {
		cdrEntity.setCallCost(10.50);
		assertEquals(10.50, cdrEntity.getCallCost());
	} 
	
	@Test
	public void getOriginNumberTest() {
		assertEquals(456, cdrEntity.getOriginNumber());
	} 
	
	@Test
	public void getDestinationNumberTest() {
		assertEquals(1, cdrEntity.getDestinationNumber());
	} 
	
	@Test 
	public void calculteCallCostTest() {
		assertEquals(2.48, cdrEntity.calculteCallCost(phoneEntity));
	} 
	
	@Test
	public void showInformationTest() {
		cdrEntity.showInformation();
	}
}
