package FacturadorTest.PersistenceTest;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Test;

import Entities.CDR;
import Entities.Phone_Number;
import Persistence.FileTxt;

public class FileTxtTest {
	Properties SQLproperties = new Properties();
	FileTxt filetxt = new FileTxt();
	List<Phone_Number> phones = new ArrayList<Phone_Number>();
	List<CDR> cdrs = new ArrayList<CDR>();
	
	@Test
	public void getTypePersistenceTest() {
		String result = filetxt.getTypePersistence();
		assertEquals("SERIALIZATED_PERSISTENCE",result);
	}
	
	@Test
	public void saveDataPhoneNumbersTest() {
		phones.add(new Phone_Number(111111,0,"PLAN_WOW", "1,2,3"));
		filetxt.saveDataPhoneNumbers(phones);
	}

	@Test
	public void saveDataCDRsTest() {
		CDR cdr = new CDR(111111, 1, "00:30:00", "10/10/10", "00:00:01");
		cdr.SetdateTarification("20/10/30");
		cdr.setCallCost(30);
		cdr.SethourTarification("10:12:33");
		cdrs.add(cdr);
		filetxt.saveDataCDRs(cdrs);
	}
	
	@Test
	public void loadDataPhoneNumbersTest() {
		List<Phone_Number> result = filetxt.loadDataPhoneNumbers();
		assertTrue(!result.isEmpty());
	}
	
	@Test
	public void loadDataCDRsTest() {
		List<CDR> result = filetxt.loadDataCDRs();
		assertTrue(!result.isEmpty());
	}
	
	@Test
	public void setUrlOrDirectoryFileTest() {
		filetxt.setUrlOrDirectoryFile("localhost:9090");
		filetxt.saveDataPhoneNumbers(phones);
	}	
}