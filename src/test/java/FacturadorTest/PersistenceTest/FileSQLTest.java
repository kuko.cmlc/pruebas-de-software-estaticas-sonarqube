package FacturadorTest.PersistenceTest;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Test;

import Entities.CDR;
import Entities.Phone_Number;
import Persistence.FileSQL;

public class FileSQLTest {
    Properties SQLproperties = new Properties();
	FileSQL filesql = new FileSQL();
	List<Phone_Number> phones = new ArrayList<Phone_Number>();
	List<CDR> cdrs = new ArrayList<CDR>();
	
	@Test
	public void saveDataPhoneNumbersTest() {
		phones.add(new Phone_Number(111111,0,"PLAN_WOW", "1,2,3"));
		filesql.saveDataPhoneNumbers(phones);
	}

	@Test
	public void saveDataCDRsTest() {
		CDR cdr = new CDR(111111, 1, "00:30:00", "10/10/10", "00:00:01");
		cdr.SetdateTarification("20/10/30");
		cdr.setCallCost(30);
		cdr.SethourTarification("10:12:33");
		cdrs.add(cdr);
		filesql.saveDataCDRs(cdrs);
	}
	
	@Test
	public void loadDataPhoneNumbersTest() {
		List<Phone_Number> result = filesql.loadDataPhoneNumbers();
		assertTrue(!result.isEmpty());
	}
	
	@Test
	public void loadDataCDRsTest() {
		List<CDR> result = filesql.loadDataCDRs();
		assertTrue(!result.isEmpty());
	}
	
	@Test
	public void loadDataCDRsErrorTest() {
		filesql.setUrlOrDirectoryFile("localhost:9090");
		List<CDR> result = filesql.loadDataCDRs();
		assertTrue(result.isEmpty());
	}
	
	@Test
	public void loadDataPhoneNumbersErrorTest() {
		filesql.setUrlOrDirectoryFile("localhost:9090");
		List<Phone_Number> result = filesql.loadDataPhoneNumbers();
		assertTrue(result.isEmpty());
	}

	@Test
	public void saveDataPhoneNumbersErrorTest() {
		filesql.setUrlOrDirectoryFile("localhost:9090");
		phones.add(new Phone_Number(111111,0,"PLAN_WOW", "1,2,3"));
		filesql.saveDataPhoneNumbers(phones);
	}

	@Test
	public void saveDataCDRsErrorTest() {
		filesql.setUrlOrDirectoryFile("localhost:9090");
		CDR cdr = new CDR(111111, 1, "00:30:00", "10/10/10", "00:00:01");
		cdr.SetdateTarification("20/10/30");
		cdr.setCallCost(30);
		cdr.SethourTarification("10:12:33");
		cdrs.add(cdr);
		filesql.saveDataCDRs(cdrs);
	}
	
	@Test
	public void getPropertiesTest() {
		Properties result = filesql.getProperties("config.fakeproperties");
		assertEquals(null,result);
	}
	
	@Test
	public void getTypePersistenceTest() {
		String result = filesql.getTypePersistence();
		assertEquals("SQL_PERSISTENCE",result);
	}
}
